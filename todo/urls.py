from django.urls import path
from todo import views

app_name = "todo"

urlpatterns = [
    path("", views.index, name="index"),
    path("todos/", views.todos, name="todos"),
    path("todos/<int:pk>/", views.todo_detail, name="todo-detail"),
    path("todos/<int:pk>/edit", views.todo_edit_form, name="todo-edit-form"),
]
