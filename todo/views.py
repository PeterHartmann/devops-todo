from django.shortcuts import render, reverse, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, QueryDict
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from todo.models import Todo
from todo.forms import TodoForm


def index(request):
    return HttpResponseRedirect(reverse("todo:todos"))


@login_required
def todos(request, pk=None):
    todo_form = None

    if request.method == "POST":
        todo_form = TodoForm(request.POST)
        if todo_form.is_valid():
            todo_form.instance.user = request.user
            todo_form.save()
            todo_form = None

    elif request.method == "DELETE":
        todo = get_object_or_404(Todo, pk=pk)
        if todo.user == request.user:
            todo.delete()
        else:
            raise PermissionDenied("This todo item does not belong to you!")

    if not todo_form:
        todo_form = TodoForm()

    todos = Todo.objects.filter(user=request.user)
    context = {"todos": todos, "todo_form": todo_form}

    return render(request, "todo/index.html", context)


def todo_detail(request, pk=None):
    todo = get_object_or_404(Todo, pk=pk)
    context = {"todo": todo}
    if request.method == "GET":
        return render(request, "todo/todo.html", context)

    elif request.method == "PUT":
        data = QueryDict(request.body).dict()
        form = TodoForm(data, instance=todo)
        if form.is_valid():
            form.save()
            return render(request, "todo/partials/detail.html", context)

        context["form"] = form
        return render(request, "todo/partials/edit-todo-form.html", context)


def todo_edit_form(request, pk=None):
    todo = get_object_or_404(Todo, pk=pk)
    form = TodoForm(instance=todo)
    context = {"todo": todo, "form": form}
    return render(request, "todo/partials/todo-edit-form.html", context)


# def todos_edit(request, pk=)
